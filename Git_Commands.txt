1) Inform GIT to start tracking our project repo

git init //initialization

2) Ask git, about project repo status ?

git status

3) To track/stage the files by git,

git add fileName(s)

git add fileName1, fileName2, fileName3 

git add *.txt

git add *.*

git add .

git add -A

4) To un-track/un-stage the files by git,

git rm --cached fileName

git rm --cached fileName1, fileName2

git rm --cached *.*

5) To commit the staged files/tracked files,

git commit -m "created initial project resources & readme file"

6) To reverse the commit for the staged files/tracked files,

git restore --staged fileName1 fileName2

7) To discard the recent changes made by the developer and to not track the same,

git restore fileName1 fileName2

8) To get help on a command,

git --help restore
git help config
git config --help

9) To remove the .git init file, 

rm -rf .git

10) To know the differences when the file(s) are modified,

git diff

11) To know the history of commits/view the logs,

git log
git log --stat

12) To change the commit message

git commit --amend -m "commit message"

13) To initialize git with your UN & email account,

git config --global user.name "userName"

git config --global user.email 'userEmailAccount"

git config --list

14) Create a .gitignore file and add the file names to be ignored inside this .gitignore file

touch .gitignore

15) Cloning few online remote repos [public] 

cd to the new directory where you would like to cloneTo

git clone githubURL pathOfYourLocalPC

16) Lets learn little about Github

17) to push git repo content to github repo,

git push -u "pathOfGithub/Gitlab URL" branchName

git remote add origin https://github.com/kirangopisetty/MichelinProject1.git

18) to pull the latest code from github repo to local repo

git pull githubURL pathOfYourLocalPC

Branching concepts

18) to create a branch

git branch branchName

19) to know the list of branches

git branch

20) to move from 1 branch to another branch

git checkout branchName

21) to merge branches with master

git merge branchName

22) to push the newly created branch & its contents

git push -u githubURL branchName

23) to delete the branches on local machine

git branch -d branchName
git branch -d branchName1 branchName2 branchName3

24) to delete the branches on remote server

git push githubURL --delete branchName

25) To add remote alias,

git remote add <aliasName> <githubURL>

26) To clone the remote github URL

git clone githubURL pathonlocalmachine

27) To stash the changes in local git repo,

git stash
git stash save "meaningful msg for stashing"

git stash list

git stash apply stash@{0}

git stash drop stash@{0}
git stash clear

git stash pop [apply + clear]

git show stash
git show stash@{0}

git add . vs git add -A ==> DONE
git reset ==> DONE
git diff ==> DONE
git merge ==> DONE
merge conflicts ==> DONE
git diff branchName ==> DONE
git diff master ==> DONE
merge vs rebase ==> DONE
git tags ==> DONE

TODAY

git diff
>>HEAD

git difftool
git mergetool

github

email service


git config --global difftool.diffmerge.cmd "C:/Program\ Files/SourceGear/Common/DiffMerge/sgdm.exe \"$LOCAL\" \"$REMOTE\""

git config --global mergetool.diffmerge.cmd "C:/Program\ Files/SourceGear/Common/DiffMerge/sgdm.exe -merge -result=\"$MERGED\" \"$LOCAL\" \"$BASE\" \"$REMOTE\""














